from machine import Pin

import DS1302     #导入DS1302.py
ds = DS1302.DS1302(Pin(0),Pin(2),Pin(1))  #接口对接
from machine import SPI, Pin,PWM
import st7789
spi = SPI(0, baudrate=40000000, polarity=1, phase=0, bits=8, endia=0, sck=Pin(6), mosi=Pin(8))
display = st7789.ST7789(spi, 240, 240, reset=Pin(11,func=Pin.GPIO, dir=Pin.OUT), dc=Pin(7,func=Pin.GPIO, dir=Pin.OUT))
display.init()
display.draw_string(50, 230, "Powered by BlackWalnuut Labs.")#经典开机logo
display.fill(st7789.color565(255, 255, 255))
week=["Mon.","Tue.","Wes.","Thur.","Fri.","Sat.","Sun."]
while True:
    d=ds.DateTime()
    date1=("%02d"%d[0])+"-"+("%02d"%d[1])+"-"+("%02d"%d[2])+" "+week[d[3]]
    display.draw_string(10, 10,date1,size=3,color=st7789.color565(0, 0, 0))#上方显示年月日星期
    date2=("%02d"%d[4])+":"+("%02d"%d[5])
    display.draw_string(15, 70,date2,size=8,color=st7789.color565(0, 0, 0))
    display.draw_string(15, 70,date2,size=8,color=st7789.color565(0, 0, 0))#正中央显示时分
    display.draw_string(190, 130,("%02d"%d[6]),size=5,color=st7789.color565(0, 0, 0))#在屏幕右下方显示秒