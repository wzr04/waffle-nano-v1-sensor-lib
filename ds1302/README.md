# 电子表

## 案例展示

&emsp;&emsp;一个相当简易的电子表，读取ds1302元件的时间数据并显示在屏幕。

![IMG_20210718_092016](/ds1302/img/IMG_20210718_092016.jpg)

## 物理连接

### 传感器选择

&emsp;&emsp; 传感器选择如下图所示的型号为ds1302的低功耗实时时钟芯片。

![IMG_20210717_214301](/ds1302/img/IMG_20210717_214301.jpg)

### 传感器接线

&emsp;&emsp;传感器与Waffle Nano 之间的接线方式如下表所示，且未在下表中显示的引脚均处于悬空不连装态。

|Waffle Nano|传感器|
|---|---|
|3v3|Vcc|
|IO0|CLK|
|IO1|RST|
|GND|GND|
|IO2|DAT|


## 传感器库使用

&emsp;&emsp;可以获取[DS1302.py](/ds1302/code/DS1302.py),将此库通过[Waffle Maker](https://wafflenano.blackwalnut.tech/ide/index.html#/editor) 的文件上传功能将此库上传到`Waffle Nano`上。

&emsp;&emsp;我们在可以在主函数中使用以下代码导入此库。

```python
 import DS1302 
```

&emsp;&emsp;在对象构造函数中，我们需要传入一个已经构造好的`DS1302`对象

```python

ds = DS1302.DS1302(Pin(0),Pin(2),Pin(1))  #构造DS1302对象，连接方式参考PWM和GPIO等接口文档
```

&emsp;&emsp;我们使用DS1302对象的`DateTime()`方法读取出一个长度为7的一维数组。

```python
d=ds.DateTime() #从ds中获取数据,其中主要的数据：d[0]为年，d[1]为月d[2]为日期，d[3]为星期几，d[4]为小时，d[5]为分钟，d[6]为秒
```
&emsp;&emsp;我们使用DS1302对象的各种函数分别改变寄存器的值。

```python
ds.Minute(x)
ds.Hour(x)
ds.Second(X)
ds.Year(x)
ds.Day(x)
ds.Month(x)
ds.Weekday(x)#其中x为你想设置的值
```

## 案例代码复现

&emsp;&emsp;可以获取[main.py](/ds1302/code/main.py)函数，将其内容复制到[Waffle Maker](https://wafflenano.blackwalnut.tech/ide/index.html#/editor) 编辑器上传输给`Waffle Nano`，以复现此案例。

&emsp;&emsp;案例相关细节说明详见代码注释

PS：本文档由本人自己编辑，可能会有错误和遗漏之处，仅供参考。