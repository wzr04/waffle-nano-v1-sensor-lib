from utime import sleep_ms


# 定义函数
def bit(data):
    return 1 << data


def delay(time):
    sleep_ms(time)


# 定义PAJ7620U2类
class PAJ7620U2:
    # 初值
    # 寄存器地址
    bank_register = 0xEF
    gesture_register = 0x43
    proximity_register = 0x44

    # PAJ7620U2模式
    gesture_mode = 0
    proximity_mode = 1
    suspend_mode = 2
    resume_mode = 3

    recent_mode = 0

    # 手势参数
    up = bit(0)
    down = bit(1)
    left = bit(2)
    right = bit(3)
    forward = bit(4)
    backward = bit(5)
    clockwise = bit(6)
    counterclockwise = bit(7)
    wave = bit(8)
    _wave = bit(0)

    leave = 0
    approach = bit(0)

    recent_gesture = 0

    # 延迟设置
    detection_delay = 50
    quit_delay = 450

    # 传感器初始化数值
    initialization_register_array = ([0xEF, 0x00],
                                     [0x37, 0x07],
                                     [0x38, 0x17],
                                     [0x39, 0x06],
                                     [0x42, 0x01],
                                     [0x46, 0x2D],
                                     [0x47, 0x0F],
                                     [0x48, 0x3C],
                                     [0x49, 0x00],
                                     [0x4A, 0x1E],
                                     [0x4C, 0x20],
                                     [0x51, 0x10],
                                     [0x5E, 0x10],
                                     [0x60, 0x27],
                                     [0x80, 0x42],
                                     [0x81, 0x44],
                                     [0x82, 0x04],
                                     [0x8B, 0x01],
                                     [0x90, 0x06],
                                     [0x95, 0x0A],
                                     [0x96, 0x0C],
                                     [0x97, 0x05],
                                     [0x9A, 0x14],
                                     [0x9C, 0x3F],
                                     [0xA5, 0x19],
                                     [0xCC, 0x19],
                                     [0xCD, 0x0B],
                                     [0xCE, 0x13],
                                     [0xCF, 0x64],
                                     [0xD0, 0x21],
                                     [0xEF, 0x01],
                                     [0x02, 0x0F],
                                     [0x03, 0x10],
                                     [0x04, 0x02],
                                     [0x25, 0x01],
                                     [0x27, 0x39],
                                     [0x28, 0x7F],
                                     [0x29, 0x08],
                                     [0x3E, 0xFF],
                                     [0x5E, 0x3D],
                                     [0x65, 0x96],
                                     [0x67, 0x97],
                                     [0x69, 0xCD],
                                     [0x6A, 0x01],
                                     [0x6D, 0x2C],
                                     [0x6E, 0x01],
                                     [0x72, 0x01],
                                     [0x73, 0x35],
                                     [0x74, 0x00],
                                     [0x77, 0x01])

    # 手势模式数据
    gesture_register_array = ([0xEF, 0x00],
                              [0x41, 0x00],
                              [0x42, 0x00],
                              [0xEF, 0x00],
                              [0x48, 0x3C],
                              [0x49, 0x00],
                              [0x51, 0x10],
                              [0x83, 0x20],
                              [0x9f, 0xf9],
                              [0xEF, 0x01],
                              [0x01, 0x1E],
                              [0x02, 0x0F],
                              [0x03, 0x10],
                              [0x04, 0x02],
                              [0x41, 0x40],
                              [0x43, 0x30],
                              [0x65, 0x96],
                              [0x66, 0x00],
                              [0x67, 0x97],
                              [0x68, 0x01],
                              [0x69, 0xCD],
                              [0x6A, 0x01],
                              [0x6b, 0xb0],
                              [0x6c, 0x04],
                              [0x6D, 0x2C],
                              [0x6E, 0x01],
                              [0x74, 0x00],
                              [0xEF, 0x00],
                              [0x41, 0xFF],
                              [0x42, 0x01])

    # 接近模式数据
    proximity_register_array = ([0xEF, 0x00],
                                [0x41, 0x00],
                                [0x42, 0x02],
                                [0x48, 0x20],
                                [0x49, 0x00],
                                [0x51, 0x13],
                                [0x83, 0x00],
                                [0x9F, 0xF8],
                                [0x69, 0x96],
                                [0x6A, 0x02],
                                [0xEF, 0x01],
                                [0x01, 0x1E],
                                [0x02, 0x0F],
                                [0x03, 0x10],
                                [0x04, 0x02],
                                [0x41, 0x50],
                                [0x43, 0x34],
                                [0x65, 0xCE],
                                [0x66, 0x0B],
                                [0x67, 0xCE],
                                [0x68, 0x0B],
                                [0x69, 0xE9],
                                [0x6A, 0x05],
                                [0x6B, 0x50],
                                [0x6C, 0xC3],
                                [0x6D, 0x50],
                                [0x6E, 0xC3],
                                [0x74, 0x05])

    # 挂起模式数据
    suspend_register_array = ([0xEF, 0x01],
                              [0x72, 0x00],
                              [0xEF, 0x00],
                              [0x03, 0x01])

    # 恢复模式数据
    resume_register_array = ([0xEF, 0x01],
                             [0x72, 0x01])

    # 初始化
    def __init__(self, i2c, address):
        delay(700)  # 等待PAJ7620U2稳定

        self.i2c = i2c
        self.address = address  # 设置需要通信的从机地址

        self.select_bank(0)

        initialization_status = self.read(0x00, 1)[0]
        if initialization_status == 0x20:  # 检测是否被唤醒
            print("Wake up finished.")
        else:
            print("Wake up failed.")
            return

        for item in self.initialization_register_array:  # 写入初始数据
            self.write(item[0], item[1])

        self.select_bank(0)

        self.change_mode(self.gesture_mode)
        print("Initialization finished.")

    # 读取寄存器数据
    def read(self, register, length):
        return self.i2c.readfrom_mem(self.address, register, length)

    # 写入寄存器数据
    def write(self, register, data):
        return self.i2c.writeto_mem(self.address, register, bytes([data]))
        # return self.i2c.write(self.address, bytes([register, data])) # 若writeto_mem方法失效则启用该行

    # 寄存器分块选择
    def select_bank(self, bank):
        self.write(self.bank_register, bank)

    # 传感器模式选择
    def change_mode(self, mode):
        if mode == self.gesture_mode:  # 手势模式
            for item in self.gesture_register_array:
                self.write(item[0], item[1])

            self.select_bank(0)

        elif mode == self.proximity_mode:  # 接近模式
            for item in self.proximity_register_array:
                self.write(item[0], item[1])

            self.select_bank(0)

        elif mode == self.suspend_mode:  # 挂起模式
            for item in self.suspend_register_array:
                self.write(item[0], item[1])

            self.select_bank(0)

        elif mode == self.resume_mode:  # 恢复模式
            for item in self.resume_register_array:
                self.write(item[0], item[1])

            self.select_bank(0)

    # 获取手势数据
    def get_gesture(self):
        if self.recent_mode == self.gesture_mode:  # 手势模式
            data = self.read(self.gesture_register, 1)[0]  # 获取原始数据

            if data:
                return data

            else:
                data = self.read(self.gesture_register+1, 1)[0]
                if data:
                    return data

        elif self.recent_mode == self.proximity_mode:  # 接近模式
            data = self.read(self.proximity_register, 1)[0]  # 获取原始数据

            if data != self.recent_gesture:
                self.recent_gesture = data

                if data == self.approach:
                    return self.approach
                else:
                    return self.leave
