from machine import I2C, Pin, SPI
from st7789 import ST7789
from paj7620u2 import PAJ7620U2, delay


# 构造IIC对象，将1号引脚设置为scl，将0号引脚设置为sda
i2c = I2C(1, sda=Pin(0), scl=Pin(1), freq=100000)
address = i2c.scan()[0]

# 构建SPI对象
spi = SPI(0, baudrate=40000000, polarity=1, phase=0,
          bits=8, endia=0, sck=Pin(6), mosi=Pin(8))

# 构建ST7789对象
screen = ST7789(spi, 240, 240, reset=Pin(
    11, func=Pin.GPIO, dir=Pin.OUT), dc=Pin(7, func=Pin.GPIO, dir=Pin.OUT))

# 初始化屏幕
screen.init()
screen.fill(0xffff)

# 创建手势传感器对象
gesture = PAJ7620U2(i2c, address)

# 识别手势
while True:
    delay(50)
    data = gesture.get_gesture()

    if data:
        screen.fill(0xffff)

        if data == gesture.up:
            screen.draw_string(105, 110, "U P", size=2, color=0x0000)
        elif data == gesture.down:
            screen.draw_string(85, 110, "D O W N", size=2, color=0x0000)
        elif data == gesture.left:
            screen.draw_string(85, 110, "L E F T ", size=2, color=0x0000)
        elif data == gesture.right:
            screen.draw_string(75, 110, "R I G H T", size=2, color=0x0000)
        elif data == gesture.forward:
            screen.draw_string(55, 110, "F O R W A R D", size=2, color=0x0000)
        elif data == gesture.backward:
            screen.draw_string(45, 110, "B A C K W A R D", size=2, color=0x0000)
        elif data == gesture.clockwise:
            screen.draw_string(35, 110, "C L O C K W I S E", size=2, color=0x0000)
        elif data == gesture.counterclockwise:
            screen.draw_string(55, 100, "C O U N T E R", size=2, color=0x0000)
            screen.draw_string(35, 120, "C L O C K W I S E", size=2, color=0x0000)
        elif data == gesture.wave:
            screen.draw_string(85, 110, "W A V E", size=2, color=0x0000)
