

# 时钟模块

## 案例展示
![](img/20210716185257.jpg)

## 物理连接

### 传感器选择 

![](img/20210716185325.jpg)

   传感器选择如下图所示的型号为DS1307的实时时钟传感器模组。

### 传感器接线 

  传感器与Waffle Nano 之间的接线方式如下表所示，且未在下表中显示的引脚均处于悬空不连装态。

| Waffle Nano |      |
| ----------- | ---- |
| 3.3         | VCC  |
| IO0         | SDA  |
| IO1         | SCL  |
| GND         | GND  |

## 传感器库使用

​		可以获取[DS1307.py](https://gitee.com/blackwalnutlabs/waffle-nano-v1-sensor-lib/blob/master/DS1307_/code/DS1307.py),将此库通过[Waffle Maker](https://wafflenano.blackwalnut.tech/ide/index.html#/editor) 的文件上传功能将此库上传到`Waffle Nano`上。

  我们在可以在主函数中使用以下代码导入此库。

```
import DS1307
```

  在对象构造函数中，我们需要传入一个已经构造好的`IIC`对象。

```
# 此处省略IIC对象的构造过程
ds = DS1307.DS1307(i2c) #构造实时时钟对象
```

  我们使用实时时钟对象的datetime()方法读取出一个含有八个元素的元组。

```
current = ds.datetime() #从实时时钟中获取数据
```

  关于此库相关细节说明详见代码注释

## 案例代码复现

  可以获取[main.py](https://gitee.com/blackwalnutlabs/waffle-nano-v1-sensor-lib/blob/master/DS1307_/code/main.py)函数，将其内容复制到[Waffle Maker](https://wafflenano.blackwalnut.tech/ide/index.html#/editor) 编辑器上传输给`Waffle Nano`，以复现此案例。

  案例相关细节说明详见代码注释