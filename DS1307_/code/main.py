'''传入库'''
from machine import I2C, Pin,SPI
import utime
import st7789
import ds1307

'''创建对象'''
i2c = I2C(1, sda=Pin(0), scl=Pin(1),freq=400000)
ds = ds1307.DS1307(i2c)

'''屏幕的初始化'''
spi = SPI(0, baudrate=40000000, polarity=1, phase=0, bits=8, endia=0, sck=Pin(6), mosi=Pin(8))
display = st7789.ST7789(spi, 240, 240, reset=Pin(11,func=Pin.GPIO, dir=Pin.OUT), dc=Pin(7,func=Pin.GPIO, dir=Pin.OUT))
display.init()
display.draw_string(50, 230, "Powered by BlackWalnuut Labs.")

'''将屏幕背景渲染为白色'''
display.fill(st7789.color565(255, 255, 255))


while True:
    current = ds.datetime()  #获取当下的时刻元组
    year = current[0]+21  #获取年份
    month = current[1]  #获取月份
    day = current[2]  #获取日期
    date = current[3]  #获取星期
    hour = current[4]  #获取小时
    minute = current[5]  #获取分钟
    second = current[6]  #获取秒
    former = "%04d/%02d/%02d" % (year,month,day)  #格式化具体某一天
    later = "%02d:%02d:%02d" % (hour,minute,second)  #格式化一天当中的时刻
    #将当下时刻累计的步数输出在显示屏上
    display.draw_string(10, 10,former,size=3,color=st7789.color565(0, 0, 0))
    display.draw_string(10, 40,later,size=3,color=st7789.color565(0, 0, 0))
    utime.sleep(1)  #一秒获取一次时刻