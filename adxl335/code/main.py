'''导入库函数'''
import utime
import math
from machine import SPI
from machine import ADC,Pin
import st7789
import gc
'''开启垃圾自动回收机制'''
gc.enable()
'''屏幕的初始化'''
spi = SPI(0, baudrate=40000000, polarity=1, phase=0, bits=8, endia=0, sck=Pin(6), mosi=Pin(8))
display = st7789.ST7789(spi, 240, 240, reset=Pin(11,func=Pin.GPIO, dir=Pin.OUT), dc=Pin(7,func=Pin.GPIO, dir=Pin.OUT))
display.init()
display.draw_string(50, 230, "Powered by BlackWalnuut Labs.")

'''渲染屏幕'''
display.fill(st7789.color565(255, 218, 185))
display.draw_string(10, 120, "Pursuing healthy life", color=st7789.color565(0, 0, 0), bg=st7789.color565(255, 218, 185), size=2)
display.circle(120, 120, 120, st7789.color565(152,251,152))
display.circle(120, 120, 119, st7789.color565(0, 255, 127))
display.circle(120, 120, 118, st7789.color565(124, 252, 0))
display.circle(120, 120, 116, st7789.color565(0, 255, 255))
display.circle(120, 120, 114, st7789.color565(64, 224, 208))
display.circle(120, 120, 113, st7789.color565(72, 209, 204))
display.circle(120, 120, 112, st7789.color565(0, 206, 209))
display.circle(120, 120, 111, st7789.color565(30, 144, 255))
display.circle(120, 120, 109, st7789.color565(132, 112, 255)) 
display.circle(120, 120, 108, st7789.color565(123, 104 ,238))
display.circle(120, 120, 107, st7789.color565(72, 61 ,139)) 
utime.sleep(3)
'''加速度计初始化'''
x=ADC(Pin(5))
y=ADC(Pin(12))
z=ADC(Pin(13))
x.equ(ADC.EQU_MODEL_8)#设置采样频率为8
y.equ(ADC.EQU_MODEL_8)
z.equ(ADC.EQU_MODEL_8)

def xyzread(x,y,z):
    accx=x.read()
    accy=y.read()
    accz=z.read()
    acclist=[accx,accy,accz]
    return acclist

'''设定上下界的阈值'''
threshold_min = -70
threshold_max = 70

'''初始化变量'''
#cnt两次为一步
cnt = 0
#存放步数
steps = 0
#存放x,y,z轴方向的加速度
xacc = []
yacc = []
zacc = []
#存放加速度矢量值
totave = []

'''计步主程序'''
while 1:
    for a in range(12340):
        for i in range(10):
            acclist=xyzread(x,y,z)
            #测量x轴方向的加速度并尾加入相应列表中
            xacc.append(acclist[0])
            utime.sleep(0.001)
            #测量y轴方向的加速度并尾加入相应列表中
            yacc.append(acclist[1])
            utime.sleep(0.001)
            #测量z轴方向的加速度并尾加入相应列表中
            zacc.append(acclist[2])
            utime.sleep(0.001)
            #计算测量时刻的加速度矢量值
            totave.append(math.sqrt((xacc[i] * xacc[i]) + (yacc[i] * yacc[i]) + (zacc[i] * zacc[i])))
            #i等于0时无法作比较
            if i==0:
                continue
            #计算两次相邻时刻的加速度数量值之差
            dif = totave[i] - totave[i-1]
            utime.sleep(0.1)
            #如果差在阈值的上下界内，表明步数不计入
            if dif>=threshold_min and dif <=threshold_max:
                pass
            #如果差在阈值的上下界外，表明步数计入
            else:
                cnt = cnt + 1
                #走一步cnt累计两次，cnt等于1表明当下的一步未走完
                if cnt==1:
                    continue
                #cnt等于2表明已经走完一步
                elif cnt==2:
                    #周期清零
                    cnt = 0
                    #步数加一
                    steps = steps + 1
            #将当下时刻累计的步数输出在显示屏上
            display.draw_string(50, 50, "Steps: %d" % steps, color=st7789.color565(0, 0, 0), bg=st7789.color565(255, 218, 185), size=2)
            #控制两次相邻测量时刻的时间间隔
            gc.enable()
            utime.sleep(0.7)
        xacc = []#重置列表
        yacc = []
        zacc = []
        totave = []
    steps = 0 #一天过后步数自动归零
