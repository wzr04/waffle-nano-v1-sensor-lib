from machine import Pin
from machine import ADC
import utime
x=ADC(Pin(5))
y=ADC(Pin(12))
z=ADC(Pin(13))
x.equ(ADC.EQU_MODEL_8)
x.atten(ADC.CUR_BAIS_DEFAULT)
y.equ(ADC.EQU_MODEL_8)
y.atten(ADC.CUR_BAIS_DEFAULT)
z.equ(ADC.EQU_MODEL_8)
z.atten(ADC.CUR_BAIS_DEFAULT)

def xyzread(x,y,z):
    accx=x.read()
    accy=y.read()
    accz=z.read()
    acclist=[accx,accy,accz]
    return acclist
