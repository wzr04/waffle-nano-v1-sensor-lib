from machine import I2C,Pin
import utime
class SGP30:
    def __init__(self,i2c):
        self.i2c = i2c
        self.addr=i2c.scan()[0]   #设置需要通信的从机（SGP传感器）地址
        self.i2c.write(self.addr,b'\x20\x03')  #初始化
        utime.sleep(15)
    def read(self):
        def bytesToInt1(bytes):    #bytes转换成int型
            result = 0
            for b in bytes[:2]:
                result = result * 256 + int(b)
            return result
        def bytesToInt2(bytes):
            result = 0
            for b in bytes[3:]:
                result = result * 256 + int(b)
            return result
        self.i2c.write(self.addr,b'\x20\x08')  #测量空气质量
        utime.sleep(1)
        data=self.i2c.read(self.addr,4)  #从self.addr地址读取数据
        result1=bytesToInt1(data)    #数据加工
        result2=bytesToInt2(data)
        return result1,result2
