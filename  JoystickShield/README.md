# Joystick Shield游戏摇杆扩展板



## 物理连接

### 传感器选择

   本文所指的传感器为如下图所示的型号为Joystick Shield的游戏摇杆扩展板模组。

![](https://gitee.com/fancifulnarrator/waffle-nano-v1-sensor-lib/raw/master/%20JoystickShield/image/%E5%AE%9E%E7%89%A9%E5%9B%BE.jpg)

​		值的说明的是，Joystick Shield的游戏摇杆扩展板的电源有3.3V和5V两个选项和切换3.3V和5V的开关，但是WaFFle nano只能对外提供3.3V电压，因此在传感器本身没有提供开关的情况下，将切换3.3V/5V作为传感器的开/关。下文如果提到开关，一律见此处说明。



### 传感器接线

  传感器与Waffle Nano 之间的接线方式如下表所示，且未在下表中显示的引脚均处于悬空不连装态。

| Waffle Nano | 传感器 |
| ----------- | ------ |
| 3.3V        | 3.3V   |
| IO14        | A      |
| IO0         | B      |
| IO1         | C      |
| IO2         | D      |
| IO12        | X      |
| IO13        | Y      |
| GND         | GND    |

### 传感器接线示意图

​		注：传感器引脚上并无标签，如果尝试连线，请参照下图或咨询卖家。

![](https://gitee.com/fancifulnarrator/waffle-nano-v1-sensor-lib/raw/master/%20JoystickShield/image/%E5%BC%95%E8%84%9A%E5%9B%BE%EF%BC%88%E5%AE%9E%E7%89%A9%EF%BC%89.jpg)



## 传感器库使用

  该传感器在WaFFle中的库为[joystickshield.py](https://gitee.com/fancifulnarrator/joystickshield/blob/master/code/joystickshield.py),下载此库，并通过[Waffle Maker](https://wafflenano.blackwalnut.tech/ide/index.html#/editor) 的文件上传功能将此库上传到`Waffle Nano`板子上，即可引用。

​		引用该库的代码为

```python
from joystickshield import JoystickShield
```

​		想要实例化一个`JoystickShield`对象`joystick`，需要4个GPIO引脚，两个ADC引脚。构造方法如下

```python
# 引脚引用过程省略

joystick = JoystickShield(pinA, pinB, pinC, pinD, adcX, adcY)
```

​		`joystick`对象的`read()`方法返回的是一个有六个元素的数组。分别代表意思详见库文件中的注释。

```python
Nowdata=ircamera.read()
```





## 案例复现

​		本案例是一个简单的，使用摇杆操控小方块在10*10的格子上移动并显示在屏幕上的小游戏。

![](https://gitee.com/fancifulnarrator/waffle-nano-v1-sensor-lib/raw/master/%20JoystickShield/image/%E6%95%88%E6%9E%9C%E5%B1%95%E7%A4%BA.jpg)

​		首先，当开关未开启，会出现“No Signal”的提示。

![](https://gitee.com/fancifulnarrator/waffle-nano-v1-sensor-lib/raw/master/%20JoystickShield/image/%E6%97%A0%E4%BF%A1%E5%8F%B7%20.jpg)

​		当开关开启后，会先提示“Welcome”，然后在2s后正式进入游戏界面。初始位置为左上角。

![](https://gitee.com/fancifulnarrator/waffle-nano-v1-sensor-lib/raw/master/%20JoystickShield/image/%E8%BF%9E%E6%8E%A5%E6%88%90%E5%8A%9F.jpg)

![](https://gitee.com/fancifulnarrator/waffle-nano-v1-sensor-lib/raw/master/%20JoystickShield/image/%E5%BC%80%E5%A7%8B.jpg)



​		可以通过摇杆的上下左右，以及按键的对应位置操控小方块移动。

![](https://gitee.com/fancifulnarrator/waffle-nano-v1-sensor-lib/raw/master/%20JoystickShield/image/%E7%A7%BB%E5%8A%A8.jpg)

  案例介绍到此结束，可以获取函数[samplecode.py](https://gitee.com/fancifulnarrator/joystickshield/blob/master/code/samplecode.py)，在已经上传过库文件的情况下将其内容复制到[Waffle Maker](https://wafflenano.blackwalnut.tech/ide/index.html#/editor) 网页编译器上烧录给`Waffle Nano`，以复现此案例。

  库文件代码中的注释有部分数据处理方法解释和对返回值的解读。